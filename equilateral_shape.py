import math
from math import tan, pi

#create a class with vaiables side_length and num_sides and two methods perimeter and area
class EquilateralShape:
    def __init__(self, num_sides, side_length):
        self.num_sides = num_sides
        self.side_length = side_length

# define function to calculate perimeter given side_length and num_sides
    def calc_perimeter(self):
        return self.num_sides * self.side_length

    # define function to calculate area given side_length and num_sides and formula for area
    def calc_area(self):
        if self.num_sides >= 5:
            return (self.side_length ** 2 * self.num_sides) / (4 * tan(pi / self.num_sides))
        else:
            return ValueError("Number of sides must be greater than 2")


#subclass of EquilateralShape
class EquilateralTriangle(EquilateralShape):
    def __init__(self, side_length):
        super().__init__(3, side_length)


    def area(self):
        side_squared = pow(self.side_length, 2)
        return math.sqrt(3) / 4 * side_squared

class EquilateralSquare(EquilateralShape):
    def __init__(self, side_length):
        super().__init__(4, side_length)


    def area(self):
        side_squared = pow(self.side_length, 2)
        return side_squared


# create instances of the class
triangle1 = EquilateralTriangle(5)
pentagon1= EquilateralShape(5, 4)
hexagon1 = EquilateralShape(6, 5)
square1 = EquilateralSquare(5)

# print the perimeter and area of the instances
print(f"Perimeter of triangle: {triangle1.calc_perimeter()} units")
print(f"Perimeter of petagon: {pentagon1.calc_perimeter()} units")
print(f"Perimeter of hexagon: {hexagon1.calc_perimeter()} units")

# print the area of the instances
print(f"Area of pentagon: {pentagon1.calc_area()} units")
print(f"Area of hexagon: {hexagon1.calc_area()} units")
