from datetime import datetime, timedelta

# define a class and intialize state
class AutoService:
    def __init__(self, current_mileage, mileage_due, num_months, service_mileage, last_service_date):
        self.current_mileage = current_mileage
        self.mileage_due = mileage_due
        self.num_months = num_months
        self.service_mileage = service_mileage
        self.last_service_date = last_service_date

    # Keep mileage up to date
    def update_mileage(self):
        self.current_mileage = int(input("Enter the current mileage: "))
        return self.current_mileage

    # define a method to calculate the service or mileage due
    def calc_service_due(self):
        months = timedelta(days=self.num_months * 30)
        self.last_service_date = datetime.strptime(self.last_service_date, "%Y, %m, %d")

        if datetime.now() >= self.last_service_date + months:
            return True
        elif self.current_mileage - self.service_mileage >= self.mileage_due:
            return True
        else:
            return "Vehicle is not due for service at this time."

    # update service mileage and date
    def update_service_date(self):
        service_completed = True
        if service_completed:
            today = datetime.now()
            self.last_service_date = datetime.strftime(today, "%Y, %m, %d")
            self.service_mileage = self.current_mileage
            return self.last_service_date, self.service_mileage





# create an instance of the class
subaru = AutoService(7002, 5000, 4, 6542, "2023, 5, 12")
prius = AutoService(32144, 4500, 3, 27600, "2022, 12, 1")
prius.update_mileage()
jeep = AutoService(10000, 5000, 6, 5000, "2023, 5, 1")
jeep.update_service_date()



print(f"Subaru due for service: {subaru.calc_service_due()}")
print(f"Prius due for service: {prius.calc_service_due()} , {prius.current_mileage}")
print(f"Jeep due for service: {jeep.calc_service_due()}, {jeep.last_service_date}")
